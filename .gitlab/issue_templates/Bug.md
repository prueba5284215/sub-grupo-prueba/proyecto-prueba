### Descripción general
<!-- Breve resumen del bug encontrado -->
### Pasos para replicar el bug
<!-- Paso a paso que debe seguir el desarrollador para replicar el bug -->
### Datos del ambiente
<!-- Datos importantes del ambiente y del cliente relacionados al bug -->
#### Cliente
#### Servidor (Windows o Linux)
#### Versión de Apache y PHP
#### Versión de los aplicativos (AZDigital, SGP, etc..)
#### Versiones de librerias (PDFtk, wkhtmltopdf, etc..)

### ¿Cuál es el funcionamiento/comportamiento actual del bug?
<!-- Descripción del funcionamiento actual que falla -->
### ¿Cuál es el funcionamiento/comportamiento esperado del bug?
<!-- Descripción del funcionamiento esperado y la sustentación del por qué debería funcionar de esa manera -->
### Logs, adjuntos y capturas del bug
<!-- Archivos importantes que ayudan a dar contexto del bug -->
/label ~bug ~"to do"
/assign ~"@jleon"
