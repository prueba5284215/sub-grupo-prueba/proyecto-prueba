###  Nombre de la Bitácora:
<!-- 
Hacer Mención de la Bitácora, si no tiene Bitácora se coloca "N/A - PRUEBAS DE INTEGRACION"
EJEMPLO:
> GIT_20230303-144213_A Clasificaciones de las PQRSDs
> N/A - PRUEBAS DE INTEGRACION
-->
> Escribe aca...
---
### Cliente:
<!-- 
Hacer mención de que Pertenece a  "QA - Analítica" o dicho cliente si son de un cliente en especial 
EJEMPLO:
> QA - ANALITICA
> MINCIENCIAS - QA ANALAITICA
-->
> Escribe aca...
---
### Servidor (Windows, Linux o Local):
<!-- 
Datos importantes del ambiente y del cliente relacionados al bug o mejora . Si sus pruebas son en local los datos de su local serian lo agregar, si son errores sobre ambiente de cliente agregar estos. Por favor no inventar datos. 
EJEMPLO:
> Windows_11 - Pro (Local)
> Linnux Centos 7.8.2003 - Test Operaciones
-->
> Escribe aca...
---
### Versión de Apache y PHP:
<!-- 
Versión de Apache y PHP que se están usando en dicho ambiente
EJEMPLO:
> - **APACHE:** #### 
> - **PHP:** ####.
-->
> Escribe aca...
---
###  Versión de los aplicativos (AZDigital, SGP, etc..):
<!-- 
Versión de los productos en lo cuales se están usando, Importante agregar toda la información:
EJEMPLO:
> **PQRSD GOBIERNO CENTRAL V3.O**   
-- RAMA: release/v3.1.0  
-- COMMIT: 18c3f95cf771aa87cbd9bf7f05ec015a055ffb8f  
-- FECHA: Tue Apr 18 12:07:35 2023 -0500  
-- REV_BD: 20230307-151533-32edb7-76893574$  
-- FECHA_BD: 2023-03-07 15:15:33$   

>**VERSIONES SGP V4.0**   
-- RAMA: release  
-- REVISION: 6977  
-- FECHA: 2023-04-05 16:15:22 -0500 (Wed, 05 Apr 2023)  
-- REV_BD: 6888$  
-- FECHA_BD: 2023-02-27 14:30:05 -0500 (Mon, 27 Feb 2023) $0   

>**VERSIONES AZDIGITAL V6.0**   
-- RAMA: release  
-- REVISION: 23670  
-- FECHA: 2023-03-31 17:31:06 -0500 (Fri, 31 Mar 2023)  
-- REV_BD: 23597$  
-- FECHA_BD: 2023-03-15 14:45:48 -0500 (mié., 15 mar. 2023) $ 
-->
> Escribe aca...
---
### Logs, adjuntos del bug
<!-- 
    Agregar el documento de evidencia error y de ser necesario los LOGS, NO imágenes. Hay un platilla para el reporte dodon explicamos el paso a paso.
-->
---

---

