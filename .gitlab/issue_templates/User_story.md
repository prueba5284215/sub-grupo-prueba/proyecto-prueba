## Título

**Cómo** [Usuario interesado en la historia]
**Quiero** [Objetivo de la historia]
**De modo que** [Razón de la historia]

## Criterio de aceptación

1. [Si hago esto, entonces..]
2. [Debería manejarse así..]
3. [Cuando pasa esto, entonces..]

## Recursos

* Wireframes: [URL del recurso o imagen para mejor entendimiento]
* Mockups: [URL del recurso o imagen para mejor entendimiento]

## Notas

Notas complementarias de ser necesario
Información relevante que se tenga para el desarrollo de la historia